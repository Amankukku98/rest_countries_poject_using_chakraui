import axios from "axios";

export const getCountries = () => {
    return axios.get('https://restcountries.com/v2/all')
        .then(res => res.data);
}

export const getRegions = (name) => {
    return axios.get(`https://restcountries.com/v2/name/${name}`)
        .then(res => res.data);

}


export const getEachCountries = (name) => {
    return axios.get(`https://restcountries.com/v2/alpha/${name}`)
        .then(res => res.data);

}
