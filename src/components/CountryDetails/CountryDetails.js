import React, { Component } from 'react'
import * as CountryApi from '../../api';
import { Link } from 'react-router-dom';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import { Box, Button, Flex, Heading, Image } from '@chakra-ui/react';
class CountryDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            countryData: '',
            isLoading: true,
            hasError: false,

        }
    }
    async componentDidMount() {
        const { name } = this.props.match.params;
        try {
            const data = await CountryApi.getRegions(name)
            this.setState({
                countryData: data[0],
                isLoading: false,
                hasError: false,
            })
        } catch (err) {
            this.setState({
                countryData: '',
                isLoading: false,
                hasError: "Failed to get data",
            })
        }

    }
    async componentDidUpdate(prevProps) {
        if (prevProps.match.params.name !== this.props.match.params.name) {
            const { name } = this.props.match.params;
            try {
                const data = await CountryApi.getEachCountries(name)
                this.setState({
                    countryData: data,
                    isLoading: false,
                    hasError: false,
                })
            } catch (err) {
                this.setState({
                    countryData: '',
                    isLoading: false,
                    hasError: "Failed to get data",
                })

            }
        }
    }

    render() {
        const { countryData } = this.state;
        if (countryData !== '') {
            console.log(countryData.currencies)
            return (
                <>
                    <Box bg=" rgb(197, 192, 192)" w="5%" ml={12} mt={12}>
                        <Link to="/">
                            <Button colorScheme='gray.400' color="black"><ArrowBackIcon />Back</Button>
                        </Link>
                    </Box>
                    <Flex wrap="wrap" p={1}>
                        <Image src={countryData.flag} alt="" w="600px" m={12} />
                        <Box p={12} mt={58} lineHeight={2}>
                            <Heading fontSize={28} mb={8}>{countryData.name}</Heading>
                            <p>Native Name:{countryData.nativeName}</p>
                            <p>Population:{countryData.population}</p>
                            <p>Region:{countryData.region}</p>
                            <p>Sub Region:{countryData.subregion}</p>
                            <p>Capital:{countryData.capital}</p>
                        </Box>
                        <Box p={3} mt={155} mr={12} lineHeight={2}>
                            <p>Top Level Domain:{countryData.topLevelDomain}</p>
                            <p>Currencies:{countryData.currencies[0].name}</p>
                            <p>Languages:{countryData.languages[0].name}</p>

                        </Box>
                        <Box display="flex" mt="-3%" ml="39%">
                            <Heading as="h4" fontSize={20}> Border Countries:</Heading>
                            {countryData.borders === undefined ? <p>No Borders</p> : countryData.borders.map((item, index) => {
                                return (
                                    <Link key={index} to={item}>
                                        <Button m={2} mt={-2} p={5} w={40} bg="gray.200" color="gray.600">{item}</Button>
                                    </Link>
                                )
                            })}
                        </Box>
                    </Flex>
                </>
            )
        }
    }
}



export default CountryDetails;
