
import { Search } from '@mui/icons-material';
import React, { Component } from 'react';
class DropDown extends Component {
    render() {
        const { region, searchRegion } = this.props;
        return (
            <option value={region}>{region}</option>
        )
    }
}
export default DropDown;