import React, { Component } from "react";
import Country from '../Country/country';
import DropDown from "../dropdown/DropDown";
import SearchIcon from '@mui/icons-material/Search';
import * as CountryApi from '../../api';
import { Box, Flex, Input } from '@chakra-ui/react';

class Layout extends Component {

    constructor(props) {
        super(props);

        this.state = {
            countries: [],
            isLoading: true,
            searchCountry: '',
            searchRegion: '',
            hasError: false,

        };
    }

    componentDidMount() {
        CountryApi.getCountries()
            .then(countries => {
                this.setState({
                    countries,
                    isLoading: false,
                    hasError: false,
                });
            })
            .catch((err) => {
                this.setState({
                    countries: [],
                    isLoading: false,
                    hasError: "Failed to load countries",
                });
            })
    }

    searchCountry = (event) => {
        this.setState({ searchCountry: event.target.value });
    }

    searchRegion = (event) => {
        this.setState({ searchRegion: event.target.value });
    }

    render() {

        const { countries, searchCountry, searchRegion } = this.state;
        let countryData = countries.filter((country => {
            return searchRegion ? country.region === searchRegion : true;
        }))

        countryData = countryData.filter((country => {
            return searchCountry ? country.name.toLowerCase().includes(searchCountry.toLocaleLowerCase()) : true;
        }))

        if (this.state.hasError) {
            return (
                <div>
                    <h1>{this.state.hasError}</h1>
                </div>
            )
        }


        if (this.state.isLoading) {
            return (
                <div>
                    <h1>Loading...</h1>
                </div>
            );
        }


        return (
            <>

                <Flex justify="space-between" mt={10} ml={12} mr={12} fontSize={20}>

                    <Box bg="aliceblue" p="3" mt="1" display="flex">
                        <SearchIcon bg="aliceblue" />
                        <Input type="text" placeholder='Search for a country...' className='search' onChange={this.searchCountry} variant='unstyled' size='md' />
                    </Box>
                    <select onChange={event => { this.searchRegion(event) }}>
                        {['Filter by Region', 'Africa', 'Americas', 'Asia', 'Europe', 'Oceania', 'Polar', 'Antarctic Ocean', 'Antarctic'].map((el, index) => {
                            return (<DropDown region={el} key={index} />)
                        })}
                    </select>

                </Flex>


                <Flex wrap="wrap">
                    {countryData.map((country) => {
                        return (
                            <Box key={country.name} >
                                <Country {...country} key={country.name} />
                            </Box>
                        );
                    })}
                </Flex>
            </>



        )
    }
}

export default Layout;