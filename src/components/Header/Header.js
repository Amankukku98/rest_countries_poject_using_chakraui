import React, { Component } from 'react';
import DarkModeIcon from '@mui/icons-material/DarkMode';
import { Box, Button, Flex, Heading, Spacer } from "@chakra-ui/react";

class Header extends Component {
    render() {
        return (
            <Flex bg="gray.300" justify="space-between" h={20} align="center">
                <Heading as="h3" ml={12}>Where in the world?</Heading>
                <Box mt={5}>
                    <DarkModeIcon /> <Button mr={12} mt={-4} id="dark">Dark Mode</Button>
                </Box>
            </Flex>

        )
    }

}

export default Header;
