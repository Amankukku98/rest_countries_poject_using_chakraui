import React from 'react'
import { Link } from 'react-router-dom';
import { Component } from "react"
import { Flex, Box, Heading, Image } from '@chakra-ui/react';
class Country extends Component {
    render() {
        const { flag, name, population, region, capital } = this.props;
        return (

            <Box bg="blue.50" h={380} w={300} mt={20} ml={12} lineHeight={2}>
                <Link to={`/${name}`} className="country-link">
                    <Image src={flag} alt="country_flag" h={200} w={300} />
                    <Box p={5}>
                        <Heading as="h6" fontSize={22} mt={5}>{name}</Heading>
                        <p>Population:{population}</p>
                        <p>Region:{region}</p>
                        <p>Capital:{capital}</p>
                    </Box>
                </Link>
            </Box>


        )
    }
}

export default Country;
